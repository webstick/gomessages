window.Messenger = function(options) {
    //chat service object
    var chatService = {
        auth: function(login, password){
            return fetch(options.api.authUrl, {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded',
                },
                method: 'POST',
                body: "login=" + login + "&password=" + password
            });
        },
        getMessages: function(token) {
            return fetch(options.api.listMessageUrl, {
                method: 'GET',
                headers: {
                    'Authorization': 'Bearer ' + token
                }
            });
        }
    };
    //


    var getContentElement = function(){
        return document.getElementById(options.el.substr(1)).getElementsByTagName("UL")[0];
    }

    var scrollToBottom = function(){
        var list = getContentElement();
        list.scrollTop = list.scrollHeight;
    }

    var scrollFromTop = function(d){
        var list = getContentElement();
        list.scrollTop = d;
    }

    var GetScrollHeight = function(){
        var list = getContentElement();
        return list.scrollHeight - list.clientHeight;
    }

    var IsScrollBottom = function(){
        var list = getContentElement();
        return list.scrollTop >= list.scrollHeight - list.clientHeight - 3;
    }

    var IsScrollTop = function(){
        var list = getContentElement();
        return list.scrollTop == 0;
    }

    // create Vue component
    let vue = new Vue({

        el: options.el,

        // data
        data: {
            ready: false,
            loading: false,
            user: {
                login: "",
                password: "",
                token: ""
            },
            newMessage: null,
            messages: [],
            ws: null,
            error: ""
        },

        created() {
        },

        // Vue Watch
        watch: {

            newMessage(value) {
            }
        },

        //Vue Methods
        methods: {
            //auth method
            close() {
                this.error = "";
                this.user.token = "";
                this.user.login = "";
                this.user.password = "";
                this.ready = false;
                this.messages = [];
            },
            //auth method
            auth() {
                this.error = "";
                this.user.token = "";
                this.messages = [];
                chatService.auth(this.user.login, this.user.password)
                .then((response) => {
                    if (response.ok) {
                        return response.json()
                    }
                    this.error = "Error auth";
                    return null;
                }).then(data => {
                    if (data){
                        this.user.token = data.token;
                        if (this.ws){
                            this.ws.close();
                        }
                        this.ws = new WebSocket(options.socket.url + "?token=" + this.user.token);
                        this.ws.onopen = (e) => {
                            this.ready = true;
                        }
                        this.ws.onclose = (e) => {
                            this.ready = false;
                            this.user.token = "";
                        }
                        this.ws.onmessage = (e) => {
                            var data = JSON.parse(e.data)
                            var isScroll = IsScrollBottom();
                            this.messages.push(data);
                            if (isScroll){
                                setTimeout(scrollToBottom,100);
                            }
                        }
                    } else if (!this.error){
                        this.error = "Error get messages";
                    }
                }).catch(error => {
                    this.error = "Error auth"
                });
            },
            //send message to server
            send() {
                if (this.newMessage != null && this.newMessage != '') {
                    if (options.validate && !options.validate(this.newMessage)){
                        return;
                    }
                    this.ws.send(JSON.stringify({
                        author: this.user.login,
                        body: this.newMessage,
                    }));
                    this.newMessage = null;
                }
            },
        },

    });

}

