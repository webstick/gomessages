package app

import (
	"./config"
	"./services/auth"
	"./services/chat"
	"./services/message"
	"./services/messages"
	"fmt"
	"github.com/nsqio/go-nsq"
	"log"
	"net/http"
)

type myMessageHandler struct{}

// HandleMessage implements the Handler interface.
func (h *myMessageHandler) HandleMessage(m *nsq.Message) error {
	log.Println("handle message")
	if len(m.Body) == 0 {
		// Returning nil will automatically send a FIN command to NSQ to mark the message as processed.
		return nil
	}
	log.Println(string(m.Body))
	//process message
	err := messages.Add(string(m.Body))
	return err
}

func Run() {
	cfg := config.New()

	//create Consumer for NSQ
	consumer, err := nsq.NewConsumer(cfg.NSQ.Topic, cfg.NSQ.Channel, nsq.NewConfig())
	if err != nil {
		log.Fatal(err)
		return
	}

	// Set the Handler for messages received by this Consumer. Can be called multiple times.
	consumer.AddConcurrentHandlers(&myMessageHandler{}, 10)
	//consumer.AddHandler(&myMessageHandler{})
	// Use nsqlookupd to discover nsqd instances.
	err = consumer.ConnectToNSQLookupd(cfg.NSQ.LookupdHost)
	if err != nil {
		log.Fatal(err)
		return
	}

	//create producer for NSQL
	producer, err := nsq.NewProducer(cfg.NSQ.ProducerHost, nsq.NewConfig())
	if err != nil {
		log.Fatal(err)
		return
	}


	authServer := auth.New(cfg.Auth.Key)
	messageServer := message.New(producer,  cfg.NSQ.Topic, auth.JwtAuthentication)
	messagesServer := messages.New(auth.JwtAuthentication)
	go func() {
		//start server for add message
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", cfg.Message.Port), messageServer))
	}()
	go func() {
		//start server for get messages
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", cfg.Messages.Port), messagesServer))
	}()

	//start websocket chat server
	chatServer := chat.NewServer(cfg.Chat.Host, producer, cfg.NSQ.Topic, auth.VerifyJwtToken)
	go chatServer.Listen()

	//start auth server
	go func() {
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", cfg.Auth.Port), authServer))
	}()

	http.Handle("/", http.FileServer(http.Dir(cfg.WebServer.HomePath)))
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", cfg.WebServer.Port), nil))
	// Gracefully stop the consumer.
	consumer.Stop()
	// Gracefully stop the producer.
	producer.Stop()
}
