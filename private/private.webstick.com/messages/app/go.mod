module private.webstick.com/messages/app

go 1.13

require (
    github.com/dgrijalva/jwt-go latest
    github.com/nsqio/go-nsq latest
	github.com/gorilla/mux latest
	golang.org/x/net/websocket latest
)