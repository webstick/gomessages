package messages

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

//messages array
var messages [] string

// new server and routes for get all messages
func New(handler func(next http.Handler) http.Handler) *http.ServeMux {
	server := http.NewServeMux()
	r := mux.NewRouter()
	r.Use(handler)
	r.HandleFunc("/", messagesHandler)
	server.Handle("/", r)
	return server
}

//list messages response struct
type Result struct {
	Status   bool     `json:"status"`
	Messages []string `json:"messages"`
}


//add message from NSQ
func Add(body string) error {
	log.Print(string(body))
	messages = append(messages, string(body))
	return nil
}

//send json response from api
func send(w http.ResponseWriter, result string){
	w.Header().Add("Content-Type", "application/json")
	_, err := fmt.Fprint(w, result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

//handler for list messages endpoint
func messagesHandler(w http.ResponseWriter, r *http.Request) {
	result, _ := json.Marshal(&Result{
		Status:   true,
		Messages: messages,
	})
	send(w, string(result))
}
