package auth

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
	"regexp"
)

var key string
var testLogin = "test"
var testPassword = "abc123"
var authStr = "!&fdkhf"

//new auth server and routes
func New(k string) *http.ServeMux {
	key = k
	server := http.NewServeMux()
	r := mux.NewRouter()
	r.HandleFunc("/", authHandler).Methods("POST")
	server.Handle("/", r)
	return server
}

//auth response struct
type Result struct {
	Status bool   `json:"status"`
	Token  string `json:"token"`
}

//auth response struct when error fired
type ErrorResult struct {
	Status bool   `json:"status"`
	Error  string `json:"error"`
}

//send json response from api
func send(w http.ResponseWriter, result string){
	w.Header().Add("Content-Type", "application/json")
	_, err := fmt.Fprint(w, result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

//handler for auth endpoint
func authHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")	//get params from form
	if err := r.ParseForm(); err != nil { //if error parse form params
		w.WriteHeader(http.StatusBadRequest)
		result, _ := json.Marshal(&ErrorResult{
			Status: false,
			Error:  "Error params",
		})
		send(w, string(result))
		return
	}

	login := r.FormValue("login")
	password := r.FormValue("password")

	if login == "" || password == "" { //if login or password not exist
		w.WriteHeader(http.StatusBadRequest)
		result, _ := json.Marshal(&ErrorResult{
			Status: false,
			Error:  "Not specified login or password",
		})
		send(w, string(result))
		return
	}

	//login validation
	var regexpLogin = regexp.MustCompile(`^[a-z0-9]{3,20}$`)

	if !regexpLogin.MatchString(login) {
		w.WriteHeader(http.StatusBadRequest)
		result, _ := json.Marshal(&ErrorResult{
			Status: false,
			Error:  "Error login",
		})
		send(w, string(result))
		return
	}

	//password validation
	var regexpPassword = regexp.MustCompile(`^.{6,}$`)

	if !regexpPassword.MatchString(password) {
		w.WriteHeader(http.StatusBadRequest)
		result, _ := json.Marshal(&ErrorResult{
			Status: false,
			Error:  "Error password",
		})
		send(w, string(result))
		return
	}

	//check user by login and password
	if login != testLogin || password != testPassword {
		w.WriteHeader(http.StatusBadRequest)
		result, _ := json.Marshal(&ErrorResult{
			Status: false,
			Error:  "Error login or password",
		})
		send(w, string(result))
		return
	}

	//Create and send token
	hashed := getHashAuth(login, password)
	tk := &Token{Auth: string(hashed)}
	tokenString, _ := GetJwtToken(tk)

	result, _ := json.Marshal(&Result{
		Status: true,
		Token:  tokenString,
	})

	send(w, string(result))
}
