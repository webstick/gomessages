package auth

import (
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"net/http"
	"strings"
)

//payload token struct
type Token struct {
	Auth string
	jwt.StandardClaims
}

//get hash auth params
func getHashAuth(l string, p string) string {
	h := sha256.New()
	h.Write([]byte(l + authStr + p))
	return string(base64.StdEncoding.EncodeToString(h.Sum(nil)))
}

//handler for validate auth token before call endpoint which need auth
var JwtAuthentication = func(next http.Handler) http.Handler {

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization") //get params from form
		if r.Method == "OPTIONS" {
			return
		}
		tokenHeader := r.Header.Get("Authorization") //get token from header
		if tokenHeader == "" { //token is not exist
			w.WriteHeader(http.StatusForbidden)
			result, _ := json.Marshal(&ErrorResult{
				Status: false,
				Error: "Missing auth token",
			})
			send(w, string(result))
			return
		}

		splitted := strings.Split(tokenHeader, " ") //token format `Bearer {token-body}`
		if len(splitted) != 2 {
			w.WriteHeader(http.StatusForbidden)
			result, _ := json.Marshal(&ErrorResult{
				Status: false,
				Error: "Invalid/Malformed auth token",
			})
			send(w, string(result))
			return
		}

		tokenPart := splitted[1] //get token after "Bearer "


		tk := &Token{}

		token, err := jwt.ParseWithClaims(tokenPart, tk, func(token *jwt.Token) (interface{}, error) {
			return []byte(key), nil
		})

		if err != nil { //error token
			w.WriteHeader(http.StatusForbidden)
			result, _ := json.Marshal(&ErrorResult{
				Status: false,
				Error: "Malformed authentication token",
			})
			send(w, string(result))
			return
		}

		if !token.Valid { //invalid token error
			w.WriteHeader(http.StatusForbidden)
			result, _ := json.Marshal(&ErrorResult{
				Status: false,
				Error: "Token is not valid",
			})
			send(w, string(result))
			return
		}

		hashed := getHashAuth(testLogin, testPassword)
		if tk.Auth != hashed { //error token validation
			w.WriteHeader(http.StatusForbidden)
			result, _ := json.Marshal(&ErrorResult{
				Status: false,
				Error: "Invalid auth token",
			})
			send(w, string(result))
			return
		}

		next.ServeHTTP(w, r) //next handler
	});
}

//func create JWT token
func GetJwtToken(tk *Token) (string, error) {
	token := jwt.NewWithClaims(jwt.GetSigningMethod("HS256"), tk)
	return token.SignedString([]byte(key))
}

//validate token
func VerifyJwtToken(t string) bool {
	tk := &Token{}
	token, err := jwt.ParseWithClaims(t, tk, func(token *jwt.Token) (interface{}, error) {
		return []byte(key), nil
	})
	return err == nil && token.Valid && tk.Auth == getHashAuth(testLogin, testPassword)
}


