package message

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/nsqio/go-nsq"
	"net/http"
)

var producer *nsq.Producer
var topic string

// new server and routes for add message
func New(prod *nsq.Producer, t string, handler func(next http.Handler) http.Handler) *http.ServeMux {
	topic = t
	producer = prod
	server := http.NewServeMux()
	r := mux.NewRouter()
	r.Use(handler)
	r.HandleFunc("/", messagesHandler).Methods("POST")
	server.Handle("/", r)
	return server
}

//send json response from api
func send(w http.ResponseWriter, result string){
	w.Header().Add("Content-Type", "application/json")
	_, err := fmt.Fprint(w, result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}
}

//add message response struct
type MessageResult struct {
	Status bool `json:"status"`
}

//response struct when error add message
type MessageErrorResult struct {
	Status bool `json:"status"`
	Error string `json:"error"`
}

//handler for add message endpoint
func messagesHandler(w http.ResponseWriter, r *http.Request) {
	//get params from form
	if err := r.ParseForm(); err != nil {//if error parse form params
		w.WriteHeader(http.StatusBadRequest)
		result, _ := json.Marshal(&MessageErrorResult{
			Status: false,
			Error: "Error params",
		})
		send(w, string(result))
		return
	}


	message := r.FormValue("message")

	//validate message
	if message == "" {
		w.WriteHeader(http.StatusBadRequest)
		result, _ := json.Marshal(&MessageErrorResult{
			Status: false,
			Error: "Error message",
		})
		send(w, string(result))
		return
	}

	//add message to NSQ
	err := producer.Publish(topic, []byte(message))

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	}else{
		result, _ := json.Marshal(&MessageResult{
			Status: true,
		})
		send(w, string(result))
	}
}
