package config

import (
	"os"
)

func getEnv(key string, defaultVal string) string {
	if value, exists := os.LookupEnv(key); exists {
		return value
	}

	return defaultVal
}

//params for Authentication
type Auth struct {
	Port string
	Key  string
}

//params for add message sever endpoint
type Message struct {
	Port string
}

//params for list messages sever endpoint
type Messages struct {
	Port string
}

//params for NSQ connection
type NSQ struct {
	LookupdHost  string
	ProducerHost string
	Topic        string
	Channel      string
}

//params for websocket chat server
type Chat struct {
	Host string
}

//params for web server
type WebServer struct {
	Port string
	HomePath string
}

//params for WebServer
type Config struct {
	NSQ      NSQ
	Auth     Auth
	Message  Message
	Messages Messages
	Chat Chat
	WebServer WebServer
}

func New() *Config {
	return &Config{
		//params for NSQ connection
		NSQ: NSQ{
			getEnv("NSQ_LOOKUPD_HOST", "127.0.0.1:4161"),
			getEnv("NSQ_PRODUCER_HOST", "127.0.0.1:4150"),
			getEnv("NSQ_TOPIC", "test"),
			getEnv("NSQ_CHANNEL", "main"),
		},
		//params for Authentication
		Auth: Auth{
			getEnv("AUTH_PORT", "8080"),
			getEnv("AUTH_KEY", "ABC123"),
		},
		//params for add message server endpoint
		Message: Message{
			getEnv("MESSAGE_PORT", "8081"),
		},
		//params for list messages sever endpoint
		Messages: Messages{
			getEnv("MESSAGES_PORT", "8082"),
		},
		//params for websocket chat server
		Chat: Chat {
			getEnv("CHAT_HOST", "/chat"),
		},
		WebServer: WebServer {
			getEnv("PORT", "8380"),
			getEnv("HOME_PATH", "webroot"),
		},
	}
}
